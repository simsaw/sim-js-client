;(function (window, jQuery) {
    'use strict';

    var sim = {};

    sim.settings = { appId: ''};

    sim.appId = function (appId) {
        sim.settings.appId = appId;
    };

    sim.hasAppId = function () {
        return Boolean(sim.settings.appId);
    };

    window.sim = sim;

}(window, $));
