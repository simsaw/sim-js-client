;(function (sim, jQuery) {
    'use strict';

    if (!sim) {
        throw "not able to find module 'sim'";
    }

    if (!jQuery) {
        throw "not able to find module 'jQuery'";
    }

    //data
    function SimData(name) {
        if (!name) {
            throw "fn sim.data(collectionName): missing argument 'collection Name' ";
        }
        this._collectionName = name;
    }

    SimData.prototype.save = function () {
        var dfd = new jQuery.Deferred();

        // dfd.resolve( "success" );
        // dfd.reject( "some errot" );
        // dfd.notify( "progress... " );

        // Return the Promise so caller can't change the Deferred
        return dfd.promise();
    };

    sim.data = function (collectionName) {
        return new SimData(collectionName);
    };

}(window.sim, $));